# FPGA_Region_Generator
Java program that generates possible regions (pblocks) on an Artix-7 200T FGPA device that specified input modules can be placed in.

The program takes in a list of module names stored in a text file, the utilization information for each of the modules and
the number of interconnections that exist between each of the modules. 

The possible regions are saved to a textfile to be used by a GA solver to determine the best combination of regions(pblocks)
to be used in Vivado when implementing the desired design on an Artix-7. Each region has a cost associated with it based off
its average utilization of CLBS, DSPs and BRAM. This GA tries to minimize the cost when placing regions.


Running the program:

To run the program, use the following syntax

java region_generator startX endX startY endY microBlazeName

The 4 arguements; startX,endX,startY,endY are used to define the region that is 
to be used by the microblaze on the FPGA device. The microBlazeName arguement is 
the name used by vivado to name the microblaze in the hierarchy