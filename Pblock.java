public class Pblock {
	private int startX;
	private int startY;
	private int endX;
	private int endY;
	public Resources resources;
	
	public Pblock() {
		this.resources = new Resources();
		this.setStartX(0);
		this.setStartY(0);
		this.setEndX(0);
		this.setEndY(0);
	}
	
	public Pblock(int x1, int y1, int x2, int y2, int clbs, int dsps, int bram) {
		this.resources = new Resources(clbs,dsps,bram);
		this.setStartX(x1);
		this.setStartY(y1);
		this.setEndX(x2);
		this.setEndY(y2);
	}
	
	public int getStartX() {
		return startX;
	}
	public void setStartX(int x) {
		this.startX = x;
	}
	public int getStartY() {
		return startY;
	}
	public void setStartY(int y) {
		this.startY = y;
	}
	public int getEndX() {
		return endX;
	}
	public void setEndX(int x) {
		this.endX = x;
	}
	public int getEndY() {
		return endY;
	}
	public void setEndY(int y) {
		this.endY = y;
	}
	public Resources getResources(){
		return resources;
	}
}
