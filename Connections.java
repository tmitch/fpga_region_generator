public class Connections {
	private String moduleA;
	private String moduleB;
	private int connections;
	private double cost;
	
	public Connections(String a,String b, int connect){
		this.moduleA=a;
		this.moduleB=b;
		this.connections=connect;
		this.cost = 0;
	}
	
	public String getModuleA(){
		return moduleA;
	}
	
	public String getModuleB(){
		return moduleB;
	}
	
	public int getConnections(){
		return connections;
	}
	
	public double getCost(){
		return cost;
	}
	
	public void setConnections(int c){
		this.connections = c;
	}
	
	public void setCost(double c){
		this.cost = c;
	}
}
