public class Module {
	
	public Resources resources;
	public String name;
	public Module(){
		this.name = "";
		this.resources = new Resources();
	}
	
	public Module(String name, Resources resources){
		this.name = name;
		this.resources = resources;
	}
	
	public Module(String name, int clbs, int brams, int dsps){
		this.name = name;
		this.resources = new Resources();
		this.resources.setCLBs(clbs);
		this.resources.setBRAM(brams);
		this.resources.setDSPs(dsps);
	}
	
	public Resources getResources() {
		return resources;
	}
	
	public String getName(){
		return name;
	}
	
}