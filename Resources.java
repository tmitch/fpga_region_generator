public class Resources {
	private int CLBs;
	private int DSPs;
	private int BRAMs;
	
	public Resources() {
		this.setCLBs(0);
		this.setDSPs(0);
		this.setBRAM(0);
		
	}
	
	public Resources(int clbs, int dsps, int brams) {
		this.setCLBs(clbs);
		this.setDSPs(dsps);
		this.setBRAM(brams);
	}
	

	public int getBRAM() {
		return BRAMs;
	}

	public void setBRAM(int bRAMs) {
		BRAMs = bRAMs;
	}

	public int getDSPs() {
		return DSPs;
	}

	public void setDSPs(int dSPs) {
		DSPs = dSPs;
	}

	public int getCLBs() {
		return CLBs;
	}

	public void setCLBs(int cLBs) {
		CLBs = cLBs;
	}

}