public class Tile {
	protected final TileType type;
	protected final int capacity;
	
	public Tile(TileType type, int capacity){
		this.type = type;
		this.capacity = capacity;
	}
	
}
