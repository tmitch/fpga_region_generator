public class Region {
	private int number;
	private double cost;
	private int startX;
	private int startY;
	private int endX;
	private int endY;
	
	
	public Region(int startX,int startY,int endX,int endY,int number, double c){
		this.number = number;
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		this.cost = c;
	}
	
	public int getStartX(){
		return startX;
	}
	
	public int getStartY(){
		return startY;
	}
	
	public int getEndX(){
		return endX;
	}
	
	public int getEndY(){
		return endY;
	}
	public int getNumber(){
		return number;
	}
	
	public double getCost(){
		return cost;
	}
	
	public void setCost(double c){
		this.cost = c;
	}
}
