import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Objects;

public class region_generatorIDF {
	private static int NUM_ROWS = 5;
	private static int NUM_COLS = 106;
	private static int FPGA_WIDTH = NUM_COLS;
	private static int FPGA_HEIGHT = NUM_ROWS;
	private static double MAX_CLB_UTIL = 0.87;

	public static void main(String args[]) throws IOException{

		if(args.length == 6){
			int startX,startY,endX,endY;
			startX = Integer.parseInt(args[0]);
			endX = Integer.parseInt(args[1]);
			startY = Integer.parseInt(args[2]);
			endY = Integer.parseInt(args[3]);
			String microBlazeName = args[4];
			String directory = args[5];
		    ArrayList<Module> modules = createModules(microBlazeName,directory);

			System.out.println("Done calculating modules, # modules is: " + modules.size());
			//ArrayList<Pblock> pblocks = createPblocks(startX,startY,endX,endY);
			//System.out.println("Done calculating pblocks, # pblocks is: " + pblocks.size());

			//Calculate the regions in which modules can be placed
			ArrayList<ArrayList<Region>> regions = new ArrayList<ArrayList<Region>>();
			ArrayList<Region> r;
			int number =0;
			for(int i=0; i< modules.size();i++){
				//r= createRegions(modules.get(i),pblocks,number);
				r = createReducedRegions(modules.get(i),number,startX,startY,endX,endY);
				regions.add(r);
				number += r.size();
			}
			System.out.println("Number of regions:" + number);
			//Create microblaze module
		    Module m = new Module(microBlazeName,0,0,0);
		    modules.add(m);
		    //Add microblaze region
		    r = new ArrayList<Region>();
		    Region region = new Region(startX,startY,endX,endY,number+1,0);
			r.add(region);
			regions.add(r);

			//Normalize the region costs
			normalizeRegionCost(regions);

			//Create the connection information
			ArrayList<Connections> connections = createConnections(directory);

			//Create the problem file
			createOutputFile(directory,regions,modules,connections);
			System.out.println("File created");
		} else {
			System.out.println("Not enough arguements specified.");
			System.out.println("Need to specify startX,startY,endX,endY,mbName,directory");
		}
	}


	public static ArrayList<Module> createModules(String mbName,String directory) throws IOException{
		/*
		 * Extracts all modules from the utilization report and creates a
		 * list of them. Each module holds the amount of required BRAM,
		 * DSPs and CLBs that are needed for the module.
		 */
		ArrayList<Module> modules = new ArrayList<Module>();

		//Open the utilization file and read each module and its utilization

		try {
			String line;
			String s;
			String name;
			int slices;
			int bram;
			int dsps;
			BufferedReader in1,in2;
			in1 = new BufferedReader(new FileReader(directory + "/utilization.txt"));
			in2 = new BufferedReader(new FileReader(directory + "/modules.txt"));
			//The first line is so we can read it so skip over it
			line = in1.readLine();
			name = in2.readLine();
			while(line != null)
	        {
				// Want to not add a line for the microblaze
				if(!Objects.equals(mbName,name)){
					s= line.substring(0, line.indexOf(" "));
					//name = s;

					// Extract # slices
					s = line.substring(line.indexOf(" ")+1);
		        	slices = Integer.parseInt(s.substring(0, s.indexOf(" ")));

		        	// Extract # bram
		        	s = s.substring(s.indexOf(" ")+1);
		        	bram = Integer.parseInt(s.substring(0, s.indexOf(" ")));

		        	// Extract # dsps
		        	s = s.substring(s.indexOf(" ")+1);
		        	dsps = Integer.parseInt(s);

		        	// Create the module
		        	Module m = new Module(name,(slices+1)/2,bram,dsps);
		        	modules.add(m);
				}

	        	// Read the next line
	        	line = in1.readLine();
	        	name = in2.readLine();
	        }
			in1.close();
			in2.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return modules;
	}

	public static ArrayList<Pblock> createPblocks(int x1,int y1,int x2,int y2){
		/*
		 * Creates all possible pblocks that could exist on the Artix-7 200T device
		 */
		ArrayList<Pblock> pblocks = new ArrayList<Pblock>();
		int startX,startY,endX,endY =0;
		int clbs,bram,dsps =0;
		for(startY =0; startY < NUM_ROWS; startY++){
			for(startX=0; startX < NUM_COLS; startX++){
				for(endY=startY; endY < NUM_ROWS; endY++){
					clbs=0;
					bram=0;
					dsps=0;
					for(endX=startX;endX < NUM_COLS; endX++){
						clbs += addCLBS(endX)*(endY+1-startY);
						bram += addBRAM(endX)*(endY+1-startY);
						dsps += addDSPS(endX)*(endY+1-startY);

						//Create the new Pblock
						if(!overlappingWithMb(startX,startY,endX,endY,x1,x2,y1,y2)){
							Pblock p = new Pblock(startX,startY,endX,endY,clbs,dsps,bram);
							pblocks.add(p);
						}
					}
				}
			}
		}


		return pblocks;
	}

	public static boolean overlappingWithMb(int startX,int startY,int endX,int endY,int x1,int x2,int y1,int y2){
		/*
		 * Determines if created region will overlap with the region of the FPGA
		 * that is hold the microblaze
		 */

		//This is for if the pblock has bounds in the mb range
		boolean overlapping=false;
		if((startX <= x2 && startX >= x1) || (endX <= x2 && endX >= x1)){
			if((startY <y2 && startY >= y1) || (endY <= y2 && endY >= y1)){
				//Then the pblock rectangle overlaps with the area set aside for the microblaze
				overlapping = true;
			}
		}
		if((startX <= x2 && endX >=x1) && (startY < y1 && endY >y2)){
			//Then the corners of the pblock are not in the microblaze area but the
			overlapping = true;
		}


		return overlapping;

	}

	public static ArrayList<Region> createReducedRegions(Module m, int n,int x1,int y1,int x2,int y2){
		/*
		 * Create the feasible regions that a module can be placed in on the FPGA.
		 * Differs from the below function as it results in fewer regions to be
		 * generated overall.
		 */
		ArrayList<Region> regions = new ArrayList<Region>();
		int startX,startY,endX,endY =0;
		int clbs,bram,dsps =0;
		double totalClbs,totalDsps,totalBram,cost=0;
		int number = n+1;
		int sub;
		for(startY =0; startY < NUM_ROWS; startY++){
			for(startX=0; startX < NUM_COLS; startX++){
				for(endY=startY; endY < NUM_ROWS; endY++){
					for(endX=startX;endX < NUM_COLS; endX++){
				    //Add the region
					  clbs=0;
						bram=0;
						dsps=0;
						for(int i=startX;i<=endX;i++){
							sub =0;
							//These sub regions are regions located on the fpga
							if((i>=18 && i<=23) && (startY == 3 ||endY == 3 ||(startY <3 && endY >3))){
							    sub += -1;
							}
							if((i>=25 && i<=28) && (endY == 4)){
								sub += -1;
							}
							if((i>= 35 && i<=74) && (endY == 4)){
							    sub += -1;
							}
							if((i>= 35 && i<=74) && (startY == 0)){
							    sub += -1;
							}
							clbs += addCLBS(i)*(endY+1-startY + sub);
							bram += addBRAM(i)*(endY+1-startY + sub);
							dsps += addDSPS(i)*(endY+1-startY + sub);
							if(addCLBS(i)>0){
								clbs += -2;
							}
							if(addBRAM(i)>0){
								bram += -2;
							}
							if(addDSPS(i)>0){
								dsps += -2;
							}
						}
						if(!overlappingWithMb(startX,startY,endX,endY,x1,x2,y1,y2)){
						    //Then can check to see if module m will fit in that pblock region
							if( ((double)(m.getResources().getCLBs())/(double)clbs <MAX_CLB_UTIL) &&
								m.getResources().getDSPs()<dsps &&
								m.getResources().getBRAM()<bram &&
								validAspectRatio(startX,startY,endX,endY)){
								//Then it is a valid region
								//System.out.println("module: " +m.getResources().getCLBs() + "region:" +(double)clbs);
								//System.out.println("Ratio :" +(double)m.getResources().getCLBs()/(double)clbs);
								//Calculate the cost for the region (minimized)
								//Currently the cost is based off the average utilization
								totalClbs = 1 - ((double)(m.getResources().getCLBs())/(double)(clbs));
								totalDsps = 1 - ((double)(m.getResources().getDSPs())/(double)(dsps));
								totalBram = 1 - ((double)(m.getResources().getBRAM())/(double)(bram));
								cost = 1-totalClbs;

								//Create region and add it to the list
								Region r = new Region(startX,startY,endX,endY,number,cost);
								regions.add(r);
								number ++;
							}
						}
					}
				}
			}
		}
		return regions;
	}

	public static boolean validAspectRatio(int startX,int startY,int endX,int endY){
		boolean valid = true;
		//Check if it is too 'tall' in the y direction
		if((endY +1) - startY == 5){
			valid = false;
		}
		return valid;
	}

	public static ArrayList<Region> createRegions(Module m, ArrayList<Pblock> p,int n){
		/*
		 * Creates the feasible regions that a module could be added on the FPGA device.
		 * Each region is unique and corresponds to when a specific module is placed in
		 * a defined pblock.
		 *
		 * The cost of a region is minimized.
		 */
		ArrayList<Region> regions = new ArrayList<Region>();
		int startX,startY,endX,endY =0;
		int number = n+1;
		double clbs,dsps,bram,cost;

		for(int i=0;i<p.size();i++){

			if((((double)m.getResources().getCLBs())/p.get(i).getResources().getCLBs() <0.9) &&
				m.getResources().getDSPs()<p.get(i).getResources().getDSPs() &&
				m.getResources().getBRAM()<p.get(i).getResources().getBRAM()){

				//Then the module can be placed in the pblock and a region can be made
				startX = p.get(i).getStartX();
				startY = p.get(i).getStartY();
				endX = p.get(i).getEndX();
				endY = p.get(i).getEndY();

				//Calculate the cost for the region (minimized)
				//Currently the cost is based off the average utilization
				clbs = 1 - ((double)(m.getResources().getCLBs())/(double)(p.get(i).getResources().getCLBs()));
				dsps = 1 - ((double)(m.getResources().getDSPs())/(double)(p.get(i).getResources().getDSPs()));
				bram = 1 - ((double)(m.getResources().getBRAM())/(double)(p.get(i).getResources().getBRAM()));
				cost = (clbs+dsps+bram)/3;

				//Create region and add it to the list
				Region r = new Region(startX,startY,endX,endY,number,cost);
				regions.add(r);
				number ++;
			}
		}

		return regions;
	}

	public static ArrayList<Connections> createConnections(String directory) throws IOException{
		/*
		 * Uses the interconnections generated by nets.tcl to creates the connections that
		 * are needed for determining module placements in the solver.
		 */

		ArrayList<Connections> connections = new ArrayList<Connections>();
		//Load the connections from file
		try {
			String line,s;
			String module1,module2;
			int connectingNets,totalNets;
			double cost;
			BufferedReader in;
			in = new BufferedReader(new FileReader(directory + "/connections.txt"));
			line = in.readLine();

			totalNets = 0;
			while(line != null)
	        {
				if(!line.matches("")){
					s= line.substring(0, line.indexOf(" "));
					module1 = s;

					s = line.substring(line.indexOf(" ")+1);
		        	module2 = s.substring(0, s.indexOf(" "));

		        	// Extract # connections
		        	s = s.substring(s.indexOf(" ")+1);
		        	connectingNets = Integer.parseInt(s);
					totalNets += connectingNets;
		        	//Create the new connection
		        	Connections c = new Connections(module1,module2,connectingNets);
		        	addConnection(connections,c);
				}
				// Read the next line
	        	line = in.readLine();
	        }
			in.close();

			//Calculate the cost for each connection
			for(int i=0;i<connections.size();i++){
				cost = (double)(connections.get(i).getConnections())/(double)(FPGA_WIDTH*FPGA_HEIGHT*totalNets);
				if(Double.isNaN(cost)){
					//This is only the case if none of the modules have connections between each other
				}
				connections.get(i).setCost(cost);
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return connections;
	}
	
	public static void addConnection(ArrayList<Connections> connections, Connections newConnection){
		/*
		 * Adds a connection to the connection list if a connection between those 2 modules doesnt already exist.
		 * If a connection already exists between those 2 modules then the already existing connection then
		 * its current connections are increased by the numberof interconnections in the new connection
		 */
		boolean added=false;
		int val1,val2;
		for(int i =0;i<connections.size() && added==false;i++){
			if((Objects.equals(newConnection.getModuleA(), connections.get(i).getModuleA()) ||
				Objects.equals(newConnection.getModuleA(), connections.get(i).getModuleB())) &&
				(Objects.equals(newConnection.getModuleB(), connections.get(i).getModuleA()) ||
				Objects.equals(newConnection.getModuleB(), connections.get(i).getModuleB()))	){

				//Then add the number of interconnections to the current value in the list
				val1 = newConnection.getConnections();
				val2 = connections.get(i).getConnections();
				connections.get(i).setConnections(val1 + val2);
				added=true;
			}
		}

		if(added ==false){
			//Then add the newConnection to the list
			connections.add(newConnection);
		}
	}
	
	public static void createOutputFile(String directory, ArrayList<ArrayList<Region>> regions, ArrayList<Module> modules,ArrayList<Connections> connections){
		/*
		 * Creates the problem.txt file needed for determining the optimal solution.
		 */
		String s;
		try{
		    PrintWriter writer = new PrintWriter(directory +"/problem.txt", "UTF-8");
		    writer.println("# regions placements #");
		    for(int i=0;i<modules.size();i++){
		    	//Print the module name
		    	writer.println(modules.get(i).getName());
		    	s="";
		    	for(int j=0;j<regions.get(i).size();j++){
		    		s=s.concat(Integer.toString(regions.get(i).get(j).getNumber()));
		    		if(j != regions.get(i).size()-1){
		    			s=s.concat(" ");
		    		}
		    	}
		    	writer.println(s);
		    }
		    //Print the cliques, not used at all but it is required in the problem file
		    writer.println("# cliques #");
		    writer.println("1 2");

		    //Print the interconnection weights
		    writer.println("# interconnection weights #");
		    for(int i = 0;i<connections.size();i++){
		    	s=connections.get(i).getModuleA() + " " + connections.get(i).getModuleB() + " " + connections.get(i).getCost();
		    	writer.println(s);
		    }

		    //Print the regions
		    writer.println("# placements data #");
		    for(int i = 0;i<regions.size();i++){
		    	for(int j=0;j<regions.get(i).size();j++){
		    		s="";
		    		//Region number
		    		s=s.concat(Integer.toString(regions.get(i).get(j).getNumber()));
		    		s=s.concat(" ");

		    		//Region start x
		    		s=s.concat(Integer.toString(regions.get(i).get(j).getStartX()));
		    		s=s.concat(" ");

		    		//Region start y
		    		s=s.concat(Integer.toString(regions.get(i).get(j).getStartY()));
		    		s=s.concat(" ");

		    		//Region end x
		    		s=s.concat(Integer.toString(regions.get(i).get(j).getEndX()+1));

		    		s=s.concat(" ");

		    		//Region end y
		    		s=s.concat(Integer.toString(regions.get(i).get(j).getEndY()));
		    		s=s.concat(" ");

		    		//Region cost
		    		s=s.concat(Double.toString(regions.get(i).get(j).getCost()));

		    	    //Write line
			    	writer.println(s);
		    	}
		    }
		    writer.close();
		} catch (IOException e) {
		   // do something
		}
	}

	public static void normalizeRegionCost(ArrayList<ArrayList<Region>> regions){
		/*
		 * This function normalises reigons by dividing the total cost of
		 * each region by (max region cost * number of regions)
		 */

		for(int i=0;i<regions.size();i++){
			for(int j=0;j<regions.get(i).size();j++){
				//Where max cost in this case is 1 (so just divide by total regions)
				regions.get(i).get(j).setCost(regions.get(i).get(j).getCost()/regions.size());
			}
		}

	}

	public static int addCLBS(int col){
		//Used to determine if the current column is a column of CLBS for Artix-7 200T
		int clbs =0;
		if(col==6 || col==17 || col==28 || col==40 || col==51 || col==58 || col==69 || col==88 || col==99 ||
			col==9 || col==14 || col==31 || col==43 || col==48 || col==61 || col==66 || col==91 || col==96 ||
			col==55 || col==24 || col==0 || col==105 || col==1 || col==104){
			clbs = 0;
		} else {
			//Then must be a CLB column
			clbs = 50;
		}
		return clbs;
	}

	public static int addBRAM(int col){
		//Used to determine if the current column is a column of BRAM for Artix-7 200T
		int bram =0;
		if(col==6 || col==17 || col==28 || col==40 || col==51 || col==58 || col==69 || col==88 || col==99){
			//Then the current column is a bram column
			bram = 10;
		}
		return bram;
	}

	public static int addDSPS(int col){
		//Used to determine if the current column is a column of DSPS for Artix-7 200T
		int dsps=0;
		if(col==9 || col==14 || col==31 || col==43 || col==48 || col==61 || col==66 || col==91 || col==96){
			//Then the current column is a dsp column
			dsps = 10;
		}
		return dsps;
	}

}
