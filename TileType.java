public enum TileType {
	CLB,
	DSP,
	BRAM,
	IO,
	CONFIG,
	CLOCK_MANAGMENT,
	CLOCK_INTERC
}